<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Users extends Model {
    protected $table = 'users';
    public $timestamps = false;
    protected $fillable = [
        'id ',
        'name',
        'email',
        'email_verified_at',
        'password',
        'remember_token',
        'created_at',
        'updated_at',
    ];
}