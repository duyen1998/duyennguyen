<?php
namespace App\Repositories;
use App\Models\Migrations;
/**
 ***************************************************************************
 * Repository Assess
 ***************************************************************************
 *
 * This is a repository to query City data
 *
 ***************************************************************************
 * @author: Nhan
 ***************************************************************************
 */
class MigrationsRepository extends BaseRepository
{
    public function __construct(){
        $this->model = new Migrations();
    }
}
?>