<?php
namespace App\Repositories;
use App\Models\Failed_jobs;
/**
 ***************************************************************************
 * Repository Assess
 ***************************************************************************
 *
 * This is a repository to query City data
 *
 ***************************************************************************
 * @author: Nhan
 ***************************************************************************
 */
class Failed_jobsRepository extends BaseRepository
{
    public function __construct(){
        $this->model = new Failed_jobs();
    }
}
?>