<!DOCTYPE html>
<html lang="en">
<head>
  <title>Form DB</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

    <style>
    .form-control {
        border-radius: 11px;
        font-size: 16px;
        border: 1px solid #337ab7;
    }
    .h3, h3{
        color: #80BFEB;
        font-weight:bold;
        font-size: 30px;
    }
    .h2, h2{
        color: #E32B39;
    }
    
    .groub-form .form-group{
        margin-right: 0 !important;
        margin-left: 0 !important;
        font-size: 18px;
    }
    .btn-primary {
        font-size: 17px;
    }
    </style>
<body>

<div class="jumbotron text-center">
  <h3>CONNECT DATABASE</h3>
</div>
  
<div class="container">
  <div class="row">
  @php 
    $db = Session::get("db01");
    $db2 = Session::get("db02");
   @endphp
     @if(isset($db) && (isset($db2)))
        <form  action="{{ url('compare') }}" method="POST">
        {{ csrf_field()}}
           
            <div class="col-sm-6">
                <h2>Database 1</h2>
                <div class="form-horizontal groub-form">
                    <div class="form-group">
                        <label for="hostname">Hostname:</label>
                        <input type="text" class="form-control" id="hostname" value="{{$db['host']}}" name="database[hostname]"/>
                    </div>
                    
                    <div class="form-group">
                        <label for="port">Port:</label>
                        <input type="text" class="form-control" id="port" value="{{$db['port']}}"  name="database[port]">
                    </div>
                    <div class="form-group">
                        <label for="username">Username:</label>
                        <input type="text" class="form-control" id="username" value="{{$db['username']}}"  name="database[username]"/>
                    </div>
                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" class="form-control" id="password" value="{{$db['password']}}" name="database[password]"/>
                    </div>
                    <div class="form-group">
                        <label for="dbname">DB name:</label>
                        <input type="text" class="form-control" id="dbname" value="{{$db['dbname']}}" name="database[dbname]"/>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <h2>Database 2</h2>
                <div class="form-horizontal groub-form">                   
                    <div class="form-group">
                        <label for="hostname">Hostname:</label>
                        <input type="text" class="form-control" id="hostname2" value="{{$db2['host']}}" name="database2[hostname2]"/>
                    </div>
                
                    <div class="form-group">
                        <label for="port">Port:</label>
                        <input type="text" class="form-control" id="port2" value="{{$db2['port']}}" name="database2[port2]"/>
                    </div>
                    <div class="form-group">
                        <label for="username">Username:</label>
                        <input type="text" class="form-control" id="username2" value="{{$db2['username']}}"  name="database2[username2]"/>
                    </div>
                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" class="form-control" id="password2" value="{{$db2['password']}}"  name="database2[password2]"/>
                    </div>
                    <div class="form-group">
                        <label for="dbname">DB name:</label>
                        <input type="text" class="form-control" id="dbname2" value="{{$db2['dbname']}}" name="database2[dbname2]"/>
                    </div>
                </div>
            </div>
            <div style="text-align:center">
                    <button type="submit" class="btn btn-primary"> Cấu trúc DB</button>
            </div>
            

        </form>
    @else
    Chua co session
    <form  action="{{ url('compare') }}" method="POST">
            {{ csrf_field()}}
                <div class="col-sm-6">
                    <h3>Database 1</h3>
                    <div class="form-horizontal groub-form">                      
                        <div class="form-group">
                            <label for="hostname">Hostname:</label>
                            <input type="text" class="form-control" id="hostname"  name="database[hostname]"/>
                        </div>
                        
                        <div class="form-group">
                            <label for="port">Port:</label>
                            <input type="text" class="form-control" id="port"  name="database[port]">
                        </div>
                        <div class="form-group">
                            <label for="username">Username:</label>
                            <input type="text" class="form-control" id="username"  name="database[username]"/>
                        </div>
                        <div class="form-group">
                            <label for="password">Password:</label>
                            <input type="password" class="form-control" id="password"  name="database[password]"/>
                        </div>
                        <div class="form-group">
                            <label for="dbname">DB name:</label>
                            <input type="text" class="form-control" id="dbname"  name="database[dbname]"/>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <h3>Database 2</h3>
                    <div class="form-horizontal groub-form">                     
                        <div class="form-group">
                            <label for="hostname">Hostname:</label>
                            <input type="text" class="form-control" id="hostname2"  name="database2[hostname2]"/>
                        </div>
                    
                        <div class="form-group">
                            <label for="port">Port:</label>
                            <input type="text" class="form-control" id="port2"  name="database2[port2]"/>
                        </div>
                        <div class="form-group">
                            <label for="username">Username:</label>
                            <input type="text" class="form-control" id="username2"  name="database2[username2]"/>
                        </div>
                        <div class="form-group">
                            <label for="password">Password:</label>
                            <input type="password" class="form-control" id="password2"  name="database2[password2]"/>
                        </div>
                        <div class="form-group">
                            <label for="dbname">DB name:</label>
                            <input type="text" class="form-control" id="dbname2"  name="database2[dbname2]"/>
                        </div>
                    </div>
                </div>
                <div style="text-align:center">
                    <button type="submit" class="btn btn-primary"> Cấu trúc DB</button>
                </div>
            </form>
    @endif
                
   </div>

</body>
</html>
