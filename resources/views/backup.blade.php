<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<style>
    {
        background-image: url("/thuctap/resources/views/hinhanh1.jpg");
    }

    .h2,
    h2 {
        color: #80BFEB;
        font-weight:bold;
    }

    .groub-form .form-group {
        margin-right: 0 !important;
        margin-left: 0 !important;
    }
    
    .text_err {
        background-color: #E32B39;
        color: #FFFFFF;
    }

    .text_err2 {
        background-color: #E32B39;
        color: #FFFFFF;
    }
    .t_err{
        background-color: #BEE4E7;
    }
    
    .t_err2{
        background-color: #BEE4E7;
    }
    .fa{
        float: right;
        margin-right: 2%;
    }
    .DB-First
    {
      text-align:;
      border:1px solid #000;
    }
    .DBName{
      background:#80bfeb;
      padding: 10px 0 10px 0;
      font-weight:bold;
      margin-top:0;
      margin-left: -0.5px;
    }
    .table-bordered{
        border: 1px solid #080909;
        margin-top: 1%;
        margin-left: -0.5px;
    }
    
    body{
      font-size: 18px;
    }
    .btn{
        font-size: 20px;
    }


</style>

<body>
    <form action="{{ url('/handle-form') }}" method="POST">
        {{ csrf_field()}}
        <div class="jumbotron text-center">
            <h2>SO SÁNH DATABASE</h2>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="checkbox">
                        <label style="font-size:20px"><input type="checkbox" id="myCheck" onclick="myFunction()"> Cấu trúc </label>
                        <p id="text" style="display:none">Checkbox is CHECKED!</p>
                        <script>
                            function myFunction() {
                            var checkBox = document.getElementById("myCheck");
                            var text = document.getElementById("text");
                            if (checkBox.checked == true){
                                alert('Cấu trúc')
                            } else {
                                alert('Dữ liệu')
                            }
                            }
                        </script>
                        
                        <div class="DB-First">
                            <h3 class="DBName">Tên DB : {{$db1_name}}</h3>
                            @if( isset($table) )
                                @foreach( $table as $key => $val)
                                    <?php 
                                        $arr_property = [];
                                    ?>   
                                    <div class="DataTable table-bordered " style="background:#f2f3fa" >
                                        <h3><?php echo "Table: $key "?></h3>                               
                                    <table class="table_name">
                                        <tbody>
                                            <?php 
                                                foreach( $val as $key1 => $val1 ) {
                                                    foreach($val1 as $key_ => $val_){
                                                        array_push($arr_property, $key_);
                                                    }
                                                    break;
                                                }
                                            ?>
                                            @foreach($arr_property as $val_property)
                                                <tr>
                                                    <td>{{$val_property}}
                                                    </td>
                                                    @foreach( $val as $key1 => $val1 )
                                                    <td>
                                                        @foreach($val1 as $key_ => $val_)
                                                            @if($val_property == $key_)
                                                                <?php echo $val_ ;?>
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                            
                                        </tbody>
                                
                                    </table>
                                    </div>
                                @endforeach
                            @endif
                            
                        </div>
                    </div>
                    
                </div>
                          
                <div class="col-sm-6">
                    <div class="checkbox">
                        <label style="font-size:20px"><input type="checkbox" id="myCheck1" onclick="myFunction()"> Dữ liệu </label>
                        <p id="text1" style="display:none">Checkbox is CHECKED!</p>
                        <script>
                            function myFunction() {
                            var checkBox = document.getElementById("myCheck1");
                            var text = document.getElementById("text1");
                            if (checkBox.checked == true){
                                alert('Dữ liệu')
                            } else {
                                alert('Cấu trúc')
                            }
                            }
                        </script>

                        <div class="DB-First">
                            <h3 class="DBName"> <?php echo "Tên DB: $db2_name";?></h3>
                            @if( isset($table2) )
                            @foreach( $table2 as $key=>$val )
                                <?php 
                                    $arr_property2 = [];
                                ?> 
                            <div class="DataTable table-bordered" style="background:#f2f3fa">
                                <h3><?php echo "Table: $key "?></h3> 
                               
                                <table class="table_name">
                                        <tbody>
                                            <?php 
                                                foreach( $val as $key2 => $val2 ) {
                                                    foreach($val2 as $key_ => $val_){
                                                        array_push($arr_property2, $key_);
                                                    }
                                                    break;
                                                }
                                            ?>
                                            @foreach($arr_property2 as $val_property2)
                                                <tr>
                                                    <td>{{$val_property2}}
                                                    </td>
                                                    @foreach( $val as $key2 => $val2 )
                                                    <td>
                                                        @foreach($val2 as $key_ => $val_)
                                                            @if($val_property2 == $key_)
                                                                <?php echo $val_ ;?>
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                     
                </div>
                
            </div>
            <div style="text-align:center">
                <button type="submit" class="btn btn-primary"> Kiểm tra </button>
            </div>
    </form>
    </div>
</body>

</html>
